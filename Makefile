NAME=server-setup-cli
VERSION=0.1.0

DIRS=src
INSTALL_DIRS=`find $(DIRS) -type d 2>/dev/null`
INSTALL_FILES=`find $(DIRS) -type f 2>/dev/null`
DOC_FILES=*.md# *.txt *.rsc
MAIN_DIR=src
MAIN_FILE=main.sh

PKG_DIR=dist
PKG_NAME=$(NAME)-$(VERSION)
PKG=$(PKG_DIR)/$(PKG_NAME).tar.gz
SIG=$(PKG_DIR)/$(PKG_NAME).asc

PREFIX=/home/lparolari/temp/tyt
DOC_DIR=$(PREFIX)/docs
READ_WITH=less

pkg:
	mkdir -p $(PKG_DIR)

$(PKG): pkg
	git archive --output=$(PKG) --prefix=$(PKG_NAME)/ HEAD

build: $(PKG)

$(SIG): $(PKG)
	gpg --sign --detach-sign --armor $(PKG)

sign: $(SIG)

clean:
	rm -f $(PKG) $(SIG)

all: $(PKG) $(SIG)

test:

tag:
	git tag v$(VERSION)
	git push --tags

release: $(PKG) $(SIG) tag

install:
	for dir in $(INSTALL_DIRS); do mkdir -p "$(PREFIX)/$$dir"; done 
	for file in $(INSTALL_FILES); do cp $$file "$(PREFIX)/$$file"; done
	mkdir -p $(DOC_DIR)
	cp -r $(DOC_FILES) $(DOC_DIR)/
	echo "alias ss-cli="$(PREFIX)/$(MAIN_DIR)/$(MAIN_FILE)"" >> "${HOME}/.bash_aliases"

uninstall:
	for file in $(INSTALL_FILES); do rm -f $(PREFIX)/$$file; done
	rm -rf $(DOC_DIR)
	sed -i.bak "/alias ss-cli*=*/d" "${HOME}/.bash_aliases"


.PHONY: build sign clean test tag release install uninstall all