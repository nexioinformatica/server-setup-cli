# Authors

A project by [NEXIO INFORMATICA srl](https://www.nexioinformatica.com).

## Core Contributor

[Luca Parolari](https://gitlab.com/lparolari) &lt;luca.parolari23@gmail.com&gt;

## Contributors

None yet. [Why not be the first?](CONTRIBUTING.md)
