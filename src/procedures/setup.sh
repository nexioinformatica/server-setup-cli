#!/usr/bin/env bash
# setup-server-cli: setup
#
# This file:
#
#  Start the setup procedure for a server.
#
# Usage as a function:
#
#  source setup.sh
#  setup
#
# Usage as a command:
#
#  setup.sh
#
# Based on a template by BASH3 Boilerplate v2.3.0
# http://bash3boilerplate.sh/#authors
#
# The MIT License (MIT)
# Copyright (c) 2013 Kevin van Zonneveld and contributors
# You are not obligated to bundle the LICENSE file with your b3bp projects as long
# as you leave these references intact in the header comments of your source files.

function setup() {
  what="${1:-}" 
  actionList="${what}"

  for action in ${actionList}; do
    info "Dispatching ${action} action"
    eval "$(${action})"
  done
}


# authorize_keys

function authorize_keys() {
  while [[ $(confirm "Authorize a key?") = "1" ]]; do
    $(authorize_key)
  done
}

function authorize_key() {
  local pub_key=$(prompt " Public key")

  local path="${HOME}/.ssh"
  local file="authorized_keys"

  mkdir -p "${path}"
  echo "${pub_key}" >> "${path}/${file}" && (
    echo "Success." >&2
    info "Key authorized."
    info "  Public key: ${pub_key}" 
    info "  Locate it at ${path}/${file}"
  ) || echo "ERROR"

  
}


# add_keys

function add_keys() {
  while [[ $(confirm "Add ssh key?") = "1" ]]; do
    $(add_key)
  done
}

function add_key() {
  local bits="4096"
  local type="rsa"
  local comment=$(prompt " Comment")
  local file="${HOME}/.ssh/id_rsa_$(prompt " File name (prefix 'id_rsa_')")"
  local passphrase=$(prompt " Passphrase")

  local ssh_keygen="ssh-keygen -t ${type} -b ${bits} -C ${comment} -f ${file} -N ${passphrase}"

  eval "${ssh_keygen}" >&2 && (
    echo "Success." >&2
    info "Key created." 
    info "  Priv file: ${file:-}" 
    info "  Pub file: ${file}.pub"
  ) || echo "ERROR" >&2
}


# ssh_hosts

function add_ssh_hosts() {
  while [[ $(confirm "Add ssh host?") = "1" ]]; do
    $(add_ssh_host)
  done
}

function add_ssh_host() {
  local host="$(prompt " Host")"
  local hostname="$(prompt " Hostname")"
  local user="$(prompt " User")"

  local identity_file="${HOME}/.ssh/id_rsa_${user}_${host}"
  if [[ $(file_exists "${identity_file}") = "1" ]]; then
    identity_file=$(prompt " Identity file" "${identity_file}")
  else
    identity_file=$(prompt " Identity file")
  fi

  mkdir -p "${HOME}/.ssh"
  local filepath="${HOME}/.ssh/config"

  (
    echo "Host ${host:-}" >> "${filepath:-}" &&
    echo "    HostName ${hostname:-}" >> "${filepath:-}" &&
    echo "    User ${user:-}" >> "${filepath:-}" &&
    echo "    IdentityFile ${identity_file:-}" >> "${filepath:-}" &&
    echo "Success." >&2 
    info "Host `${host:-}` added."
    info "Locate it at ${filepath:-}"
  ) || echo "ERROR" >&2 
}


# Backup

function add_rsync_backup_scripts() {
  while [[ $(confirm "Add rsync backup script?") = "1" ]]; do
    $(add_rsync_backup_script)
  done
}

function add_rsync_backup_script() {
  local target_backup_dir="$(prompt " Target folder (e.g., /home/user/myFolder)" "${HOME}")"
  local dest_backup_dir="$(prompt " Destination folder (e.g., user@srv1:/home/user/myBackupFodler), connection in ssh mode")"
  local script_filename="$(prompt " Script filename" "${HOME}/bin/scripts/backup.sh")"

  local script_dir=$(dirname ${script_filename})
  local script_name=$(basename ${script_filename})

  local command="rsync -vrzha -e ssh ${target_backup_dir} ${dest_backup_dir}"

  mkdir -p "${script_dir:-}"
  echo "${command}" > "${script_dir}/${script_name}" &&
  chmod u+x "${script_dir}/${script_name}" && (
    echo "Success." >&2 
    info "Script created."
    info "Script source: ${command}"
    info "Locate it at ${script_dir}/${script_name}"
  ) || echo "ERROR" >&2 
}


# Cronjob

function add_cronjobs() {
  while [[ $(confirm "Add cronjob?") = "1" ]]; do
    $(add_cronjob)
  done
}

function add_cronjob() {
  local script="$(prompt " Job command or script path" "${HOME}/bin/scripts/backup.sh")"
  
  local timing_choice=$(choice "Choose the job timing " "once_a_day" "once_a_week" "twice_a_week" "once_a_month" "other")
  local timing=""
  case "${timing_choice}" in
    once_a_day)
      echo "Setting everyday at 4 a.m." >&2
      timing="0 4 * * 0-6"
      ;;
    once_a_week)
      echo "Setting every Saturday, at 4 a.m." >&2
      timing="0 4 * * 6"
      ;;
    twice_a_week)
      echo "Setting every Tuesday and every Saturday, at 4 a.m." >&2
      timing="0 4 * * 6"
      ;;
    once_a_month)
      echo "Setting every 1st of month, at 4 a.m." >&2
      timing="0 4 1 * *"
      ;;
    other)
      timing="$(prompt "Timing: format `m d dom mon dow`")"
  esac
 
  local cronfile="${HOME}/bin/crons/jobs.cron"
  mkdir -p $(dirname "${cronfile}")

  local crontab="${timing} ${script}"
  echo "${crontab}" >> "${cronfile}" &&
  crontab "${cronfile}" && (
    echo "Success." >&2 
    info "Cronjob added."
    info "Locate it at ${cronfile}"
  ) || echo "ERROR" >&2 
}


# Entry point.

if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then
  export -f setup
else
  setup "${@}"
  exit ${?}
fi
