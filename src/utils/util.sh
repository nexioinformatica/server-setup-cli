function contains() {
    local needle="${1:-}"
    local haystack="${2:-}"

    if [[ ! "${needle}" || ! "${haystack}" ]]; then
      echo "ERROR: Needle and Haystack needs to be specified" >&2
      exit 1
    fi
    [[ ${haystack} =~ (^|[[:space:]])"${needle}"($|[[:space:]]) ]] && echo 1 || echo 0
}

function confirm() {
    local q="${1:-}"

    while true; do
        read -p "${q} (y/n): " yn
        case $yn in
            [Yy]* ) echo 1; break;;
            [Nn]* ) echo 0; exit;;
            * ) echo "Please answer yes or no." >&2 ;;
        esac
    done
}

function prompt() {
    local q="${1:-}"
    local def="${2:-}"
    local qdef=""
    
    if [[ ${def} ]]; then
        qdef="${q} [${def}]: "
    else
        qdef="${q}: "
    fi

    read -p "${qdef}" answer

    if [[ "${answer}" = "" && "${def}" ]]; then
        answer="${def}"
    fi

    echo "${answer}"
}

function choice() {
    if [[ ! "${@:2}" ]]; then
        echo "ERROR: At least an option is required"  >&2
        exit 1
    fi

    echo "${1}" >&2
    
    local i=1
    for option in "${@:2}"; do
        echo "${i}) ${option}" >&2
        i=$((i+1))
    done
    i=$((i-1))

    while true; do
        read -p "Choose [1-${i}]: " choice

        local ok="1"

        if [[ "$(is_number "${choice}")" = "" ]]; then
            echo " Please enter a number" >&2
            ok="0"
        fi
        
        if [[ "${choice}" -lt "1" || "${choice}" -gt "${i}" ]]; then
            echo " Please enter a number in ranges [1, ${i}]" >&2
            ok="0"
        fi

        if [[ "${ok}" = "1" ]]; then
            break
        fi
    done
    
    local j=1
    for option in "${@:2}"; do
        if [[ "${j}" -eq "${choice}" ]]; then
            echo "${option}"
            break
        fi
        j=$((j+1))
    done
}

function is_number() {
    local n=${1}
    re="^[0-9]+$"
    if ! [[ ${n} =~ ${re} ]] ; then
        echo "0"
    else 
        echo "1"
    fi
}

function file_exists() {
    local f="${1:-}"

    if [[ ! "${f}" ]]; then
      echo "ERROR: You must specify a file." >&2
      exit 1
    fi

    [[ -f "${f}" ]] && echo 1 || echo 0
}

function directory_exists() {
    local d="${1:-}"

    if [[ ! "${d}" ]]; then
      echo "ERROR: You must specify a directory." >&2
      exit 1
    fi

    [[ -d "${d}" ]] && echo 1 || echo 0
}

if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then
  export -f contains
  export -f confirm
  export -f prompt
  export -f file_exists
  export -f choice
#else (do nothing!)

  #contains "${@}"
 # exit ${?}
fi