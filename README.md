# server-setup-cli

[![Tagged Release](https://img.shields.io/badge/release-v0-blue.svg?longCache=true)](CHANGELOG.md)
[![Development Status](https://img.shields.io/badge/status-planning-lightgrey.svg?longCache=true)](ROADMAP.md)
[![Build Status](https://img.shields.io/badge/build-unknown-lightgrey.svg?longCache=true)](https://travis-ci.org)
[![Build Status](https://img.shields.io/badge/build-pending-lightgrey.svg?longCache=true)](https://www.appveyor.com)
[![Build Coverage](https://img.shields.io/badge/coverage-0%25-lightgrey.svg?longCache=true)](https://codecov.io)

> A convenient setup script for a server.

*server-setup-cli* is a tool that you can use to setup a server following
a well structured procedure. No effort is required: just follow programmed
step and setup production server environment. 

## Table of Contents

- [Table of Contents](#table-of-contents)
- [Features](#features)
- [Requirements](#requirements)
- [Installation](#installation)
- [Usage](#usage)
- [Development](#development)
  - [Future](#future)
  - [History](#history)
  - [Community](#community)
- [Credits](#credits)
- [License](#license)

## Features

- Wizard for scheduled backup with rsync
- Wizard for scheduled status page updates
- Wizard for ssh environment setup

## Installation

```bash
git clone https://gitlab.com/nexioinformatica/server-setup-cli.git
cd server-setup-cli
make install
ss-cli
```

Please note that `ss-cli` is an alias. If the command is not yet recognized run 
```
cd ~
source .bash_aliases
```
and rerty with `ss-cli`.

## Usage

Get help
```bash
ss-cli --help
```

Start the setup procedure
```bash
# run all tasks for setup procedure
ss-cli --setup

# run specific task
ss-cli --setup --what [task]
```
task can be one of: `[authorize_keys, add_keys, add_ssh_hosts,
add_rsync_backup_scripts, add_cronjobs]`

Setup example usage
```bash
> ssh-cli -s

Authorize a key? (y/n): y
 Public key: MY PUBLIC KEY
Success.
Authorize a key? (y/n): n

Add ssh key? (y/n): y
 Comment: test
 File name (prefix 'id_rsa_'): test
 Passphrase: test1
Generating public/private rsa key pair.
Your identification has been saved in /home/lparolari/.ssh/id_rsa_test.
Your public key has been saved in /home/lparolari/.ssh/id_rsa_test.pub.
The key fingerprint is:
SHA256:lEjySXtuL5QJLQkPgnXHgtBKHhrOiA16yFyKzbwhPnU test
The key\'s randomart image is:
+---[RSA 4096]----+
|.=o.=.+          |
|+oo+.X.* .       |
|&X+  .X =        |
|XBB. E * o       |
|.o.o.   S        |
| o.    o .       |
|  .     . .      |
|         .       |
|                 |
+----[SHA256]-----+
Success.
Add ssh key? (y/n): n

Add ssh host? (y/n): y
 Host: test
 Hostname: test.com
 User: test
 Identity file: /home/lparolari/.ssh/id_rsa_test
Success.
Add ssh host? (y/n): n

Add rsync backup script? (y/n): y
 Target folder (e.g., /home/user/myFolder) [/home/lparolari]: 
 Destination folder (e.g., user@srv1:/home/user/myBackupFodler), connection in ssh mode: test:backup
 Script filename [/home/lparolari/bin/scripts/backup.sh]: 
Success.
Add rsync backup script? (y/n): n

Add cronjob? (y/n): y
 Job command or script path [/home/lparolari/bin/scripts/backup.sh]: 
Choose the job timing 
1) once_a_day
2) once_a_week
3) twice_a_week
4) once_a_month
5) other
Choose [1-5]: 1
Setting everyday at 4 a.m.
Success.
Add cronjob? (y/n): n
```

### Options

| Option | Description |
| --- | --- |
| `-s` or `--setup` | Triggers the setup procedure |
| `-c` or `--check` | Triggers the check procedure (NOT IMPLEMENTED YET) |
| `-w` or `--what` | Specify a single task to run in setup or check procedure |
| `-l [arg]` or `--log-level [arg]` | Allows to specify the log level from 0 (nothing) to 7 (debug). Recommended log level is 6 |
| `-d` or `--debug` | Force the log level to 7 |
| `-v` or `--verbose` | Prints the script as it is executed |

## Development

See [CONTRIBUTING](CONTRIBUTING.md)

### Future

See [ROADMAP](ROADMAP.md)

### History

See [CHANGELOG](CHANGELOG.md)

### Community

See [CODE OF CONDUCT](CODE_OF_CONDUCT.md)

## Credits

See [AUTHORS](AUTHORS.md) for contributors.

A big thanks to
- [cookiecutter](https://github.com/cookiecutter/cookiecutter), for the project boilerplate and in particular to [cookiecutter-git](https://github.com/NathanUrwin/cookiecutter-git)
- [bash3boilerplate](https://github.com/kvz/bash3boilerplate), for the basic script runtime.

## License

See [LICENSE](LICENSE)
